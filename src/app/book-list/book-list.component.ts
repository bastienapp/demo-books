import { Component } from '@angular/core';



type ApiResponse = {

  data: {
    attributes: Potion
  }[]
}

type Potion =
  {
    slug: string,
    name: string
  }

@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent {

  potions: Potion[] = [];

  ngOnInit(): void {
    fetch('https://api.potterdb.com/v1/potions')
      .then((response) => response.json())
      .then((response: ApiResponse) => {
        this.potions = response.data.map((eachData) => eachData.attributes)
      })
  }
}
